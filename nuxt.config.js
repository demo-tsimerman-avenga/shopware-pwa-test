import extendNuxtConfig from "@shopware-pwa/nuxt-module/config"

export default extendNuxtConfig({
  head: {
    title: "Shopware PWA",
    meta: [{ hid: "description", name: "description", content: "" }],
  },
  publicRuntimeConfig: {
    APP_BING_MAPS_KEY: process.env.APP_BING_MAPS_KEY,
    BING_MAPS_URL: process.env.BING_MAPS_URL
  }
})
